movies = ["ID4", "Dumb & Dumber", "Tremors", "The Arrival", "Face/Off", "Species"]

def movie_return(things):
    for film in things:
      if film == "Tremors":
        print film, "... so good"
      elif film == "The Arrival":
        print film, "(Charlie Sheen was outstanding)"
      elif isinstance(film, str):
        print film, "was okay"
      else:
        print "Not a thing"

movie_return(movies)
